gtsam
===
wget -O ~/Downloads/gtsam.zip https://github.com/borglab/gtsam/archive/4.0.2.zip
>
cd ~/Downloads/ && unzip gtsam.zip -d ~/Downloads/
>
cd ~/Downloads/gtsam-4.0.2/
>
mkdir build && cd build
>
cmake -DGTSAM_BUILD_WITH_MARCH_NATIVE=OFF ..
>
sudo make install -j8
